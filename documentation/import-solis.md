# Extraction des données SOLIS pour l’extranet notaires

(à partir de l’entrepôt de données spécifiques à SOLIS)

Les objets Solis et les critères sont :

![](img/Import-Solis/solis-export-001.png)

Type Demande : on récupère toutes les aides de Solis ASG (récupérables ou non)

![](img/Import-Solis/solis-export-002.png)

Et **soit** le suivi est Prêt pour Com ou instruction

![](img/Import-Solis/solis-export-003.png)

**Soit** l’aide est accordée (déci = Acco ou Admission d’urgence) et avec une date
d’échéance > Date d’effet. 
Mais sans chercher quand. Ainsi on récupère toutes les aides accordées au moins 
1 jour de droit n’importe quand (passé ou avenir)

Ce n’est qu’ensuite que l’on va dans le rapport, faire des variables pour 
distinguer les aides sociales (donc récupérables) du reste (non récupérable : APA, PCH)

![](img/Import-Solis/solis-export-004.png)

Variable 1 :

![](img/Import-Solis/solis-export-005.png)

On cherche si le nom de l’aide contient le terme « gement » et ainsi ça récupère l’aide en hébergement et l’aide foyer logement » qui sont des aides sociales récupérables.
Donc chaque aide trouvée, on sait si elle est récupérable ou pas et on l’appelle « Aide récupérable (AS en étab ou en FL) » (ça sortira dans l’avant dernière colonne du csv.

Et on compte aussi 1 pour chacune trouvée qui contient « gement »

![](img/Import-Solis/solis-export-006.png)

Reste à savoir si le béné en a au moins une parmi toutes ses aides qu’il a dans Solis (le max de ses compteurs par aide est-il = 1 ?)

![](img/Import-Solis/solis-export-007.png)

Si oui, on sait alors que si l’indiv vaut 1 ; c’est qu’il est récupérable.

![](img/Import-Solis/solis-export-008.png)

Et donc aussi, s’il vaut 1 ; on l’appelle 1SEXTREC sinon 1SEXTNONR

![](img/Import-Solis/solis-export-009.png)

Ça donne les 2 dernières colonnes du fichier produit :

![](img/Import-Solis/solis-export-010.png)
