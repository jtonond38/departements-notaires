﻿{# Formulaire de recherche #}

<div id=recherche_i>
  <form method="post" action="index.php?index=recherche" name="loginform" onsubmit="return verifForm_Recherche();">
    <div id=acte_deces>
      <table class=table_actes id=recherche_text>
        <tr>
          <td colspan=4 id=recherche_l>Acte de décès</td>
        </tr>
        <tr>
          <td colspan=4 id=obligatoire>* Saisie obligatoire</td>
        </tr>
        <tr>
          <td colspan=3><input type=text id=msg_erreur style="display: none;"></input></td>
        </tr>
        <tr>
          <td colspan=2>
            <p class=test>En cochant cette case, vous certifiez être en possession de l'acte de décès et
              être en charge de la succession de la personne recherchée.*</p>
          </td>
          <td align=right width=62px><label class="checkbox"> <input id=check_acte
              onclick="check('check_acte','etat_civil');" type=checkbox name=check_acte value="check_acte" required /> <span
              style="margin-right: 5px;"></span>
          </label></td>
          <td></td>
        </tr>
        <tr>
          <td width=420px><label for=date_deces style="margin-left: 45px;">Date de décès* (jjmmaaaa)</label></td>
          <td colspan=2 align=right><input id=date_deces class="date_deces" autocomplete=off
            onfocus="vider('date_deces');" onKeyPress="return scanTouchechiffre(event)"
            onchange="add_slashes_Recherche('date_deces')" onblur="compare_Date_acte_deces_naissance()" maxlength="10"
            type=text name=date_deces value="{{test_data.date_deces}}" tabindex="1" required /></td>
          <td><a class="info_bulle"> <img class=info src="css/images/info.png" style='cursor: help;'> <span>Ce champ ne
                peut contenir que des chiffres</span>
          </a></td>
        </tr>
        <tr>
          <td><label for="lieu_deces" style="margin-left: 45px;">Lieu de décès*</label></td>
          <td colspan=2 align=right><input id="lieu_deces" class="lieu_deces" autocomplete="off" type="text"
            name="lieu_deces" value="{{test_data.lieu_deces}}" onKeyPress="return scanTouchelettre(event)"
            tabindex="2" required /></td>
          <td><a class="info_bulle"> <img class=info src="css/images/info.png" style='cursor: help;'> <span>Indiquer la
                commune</span>
          </a></td>
        </tr>
        <tr>
          <td><label for="date_acte" style="margin-left: 45px;">Date de l'acte de décès* (jjmmaaaa)</label></td>
          <td colspan=2 align=right><input id="date_acte" class="date_acte" onfocus="vider('date_acte');"
            autocomplete="off" onKeyPress="return scanTouchechiffre(event)"
            onchange="add_slashes_Recherche('date_acte')" onblur="compare_Date_acte_deces_naissance()" type="text"
            name="date_acte" value="{{test_data.date_acte}}" tabindex="3" maxlength="10" required /></td>
          <td><a class="info_bulle"> <img class=info src="css/images/info.png" style='cursor: help;'> <span>Ce champ ne
                peut contenir que des chiffres</span>
          </a></td>
        </tr>
        <tr>
          <td><label for="destinataire_etude" style="margin-left: 45px;">Destinataire</label></td>
          <td colspan=2 align=right><input id="destinataire_etude" class="destinataire_etude" type="text"
            name="destinataire_etude" value="{{test_data.destinataire_etude}}" tabindex="4" /></td>
          <td><a class="info_bulle"> <img class=info src="css/images/info.png" style='cursor: help;'> <span>Indiquer la
                référence interne du destinataire du message</span>
          </a></td>
        </tr>
      </table>
    </div>
    <div id=etat_civil style="display: none;">
      <table class=table_actes id=recherche_text>
        <tr>
          <td colspan=3 id=recherche_l>Etat civil</td>
        </tr>
        <tr>
          <td colspan=3 id=obligatoire>* Saisie obligatoire</td>
        </tr>
        <tr>
          <td width=420px><label for="prenom_input" style="margin-left: 45px;">Prénom *</label></td>
          <td align=right><input id="prenom_input" onKeyPress="return scanTouchelettre_pren(event)" autocomplete="off"
            class="prenom_input" type="text" value="{{test_data.prenom}}" name="prenom" tabindex="5"
            required /></td>
          <td></td>
        </tr>
        <tr>
          <td><label for="prenomd_input" style="margin-left: 45px;">Deuxième Prénom</label></td>
          <td align=right><input id="prenomd_input" onKeyPress="return scanTouchelettre_pren(event)"
            class="prenomd_input" type="text" name="prenomd" value="{{test_data.prenomd}}"
            autocomplete="off" tabindex="6" /></td>
          <td align="center"><a class=prenom id=prenomP onclick="visible('prenomplus');" style='cursor: pointer;'> + </a></td>
        </tr>
        <tr id=prenomplus style="display: none;">
          <td><label for="prenomplus" style="margin-left: 45px;">Prénoms suivants</label></td>
          <td align=right><input onKeyPress="return scanTouchelettre_pren_plus(event)" name="prenomt"
            class="prenomd_input" type="text" value="{{test_data.prenomt}}" autocomplete="off" /></td>
          <td><a class="info_bulle"> <img class="info" src="css/images/info.png" style='cursor: help;'> <span>Saisir les
                prénoms séparés par des virgules</span>
          </a></td>
        </tr>
        <tr>
          <td><label for="name_input" style="margin-left: 45px;">Nom d'usage *</label></td>
          <td align=right><input autocomplete="off" id="name_input" class="name_input"
            onKeyPress="return scanTouchelettre(event)" type="text" name="nom_usage"
            value="{{test_data.nom_usage}}" tabindex="7" required /></td>
          <td><a class="info_bulle"> <img class="info" src="css/images/info.png" style='cursor: help;'
              style='cursor:help;'> <span>Correspond au nom utilisé couramment</span>
          </a></td>
        </tr>
        <tr>
          <td><label for="civil_input" style="margin-left: 45px;">Nom d'état civil</label></td>
          <td align=right><input id="civil_input" class="civil_input" type="text"
            onKeyPress="return scanTouchelettre(event)" name="nom_civil" value="{{test_data.nom_civil}}"
            autocomplete="off" tabindex="8" /></td>
          <td></td>
        </tr>
        <tr>
          <td><label for="date_naissance" style="margin-left: 45px;">Date de Naissance * (jjmmaaaa)</label></td>
          <td align=right><input id="date_naissance" onblur="compare_Date_acte_deces_naissance()" class="date_naissance"
            value="{{test_data.date_naissance}}" onKeyPress="return scanTouchechiffre(event)"
            onchange="add_slashes_Recherche('date_naissance')" onfocus="vider('date_naissance')" maxlength="10"
            type=text autocomplete="off" name="date_naissance" tabindex="9" required /></td>
          <td><a class="info_bulle"> <img class="info" src="css/images/info.png" style='cursor: help;'> <span>Ce champ
                ne peut contenir que des chiffres</span>
          </a></td>
        </tr>
        <tr>
          <td colspan=2 align=center><input type="submit" tabindex="9" name="login" id=submit_rechercher
            value="Recherche" /></td>
          <td></td>
        </tr>
      </table>
    </div>
  </form>
</div>
