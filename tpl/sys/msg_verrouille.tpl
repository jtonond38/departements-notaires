
<div id="verouille">
  <div id="table_verouille">
    <table id="table_verouille" width="600" height="90">
      <tr>
        <td>
          Votre compte est verrouillé. Merci de suivre ce lien pour le déverrouiller :
          <br/>
        </td>
      </tr>
      <tr>
        <td>
          <a class="deverouille" href="index.php?index=mail_deverouille&login={{user_login}}&email={{user_email}}">&gt; Déverrouillage du compte</a>        
        </td>
      </tr>
    </table>
  </div>
</div>

