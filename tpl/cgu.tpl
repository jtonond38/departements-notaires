
<div id="encart_aide">
  <div id="texte_aide">
    <h1>Conditions générales d'utilisation</h1>
    <ol align="justify">
      <p class="aide" align="justify">
        Le présent document a pour objet de définir les conditions d'accès et d'utilisation générale de l'application 
        <b>{{config.nom_application}}</b>.
        Conformément aux dispositions de l'article 4 de l'ordonnance n°2005-1516 du 8 décembre 2005, il s'impose à tout
        utilisateur de l'application.
      </p>
      <h3>Toute utilisation de l'application suppose la consultation et l'acceptation des présentes conditions générales
        d'utilisation (CGU).</h3>

      <h3>Définition et objet</h3>
      <li align="justify">Le site <b>{{config.nom_application}}</b> nommé ci-après l'application, disponible à l'adresse
        {{config.adresse_url_appli}}, est mis en &oelig;uvre par le Département du Rhône en vue de
        permettre aux Notaires du département du Rhône, nommés utilisateur dans ce document, d'effectuer des
        recherches.
      </li>
      <li align="justify">L'utilisation de <b>{{config.nom_application}}</b> est réservée à l'usage professionnel des
        études notariales en charge d'une succession, et s'interrogeant sur l'existence d'une créance du Conseil
        Départemental au titre de l'aide sociale.
      </li>

      <h3>Fonctionnalité et périmètre</h3>
      <li align="justify"><b>{{config.nom_application}}</b> offre aux notaires du Département du Rhône la
        possibilité d'interroger les bases de données du Département du Rhône au sujet des
        personnes dont ils gèrent les successions pour savoir s'ils ont bénéficiés ou non d'aide récupérable par le
        Département du Rhône.</li>
      <li>La réponse apportée par le Département est soumise à l'exactitude des renseignements fournis par le notaire
        lors de la recherche d'individu, en concordance avec l'état civil de ce dernier.</li>
      <li><b>{{config.nom_application}}</b> est la voie unique de recherche, pour tout notaire du département du
        Rhône, qu'il soit sur le territoire du Conseil départemental du Rhône
        ou de la métropole de Lyon. Les sollicitations par courriel ou par papier ne sont plus traitées.</li>
      <li><b>{{config.nom_application}}</b> a été créée par le Conseil départemental du Rhône. Pour
        les personnes inconnues du Département du Rhône, cette réponse ne dispense pas de
        questionner la métropole.</li>

      <h3>Modalités d'accès et d'utilisation de l'application</h3>
      <li align="justify">L'accès à l'application est ouvert uniquement aux notaires du Rhône.</li>
      <li align="justify">La création des comptes, inhérent à chaque étude notariale du Rhône,
        revient au Département. La chambre des notaires du Rhône s'engage à prévenir le Département
        du Rhône de chaque modification.</li>
      <li>L'identifiant correspond au code CRPCEN de chaque étude.</li>
      <li align="justify">Le mot de passe (envoyé par mail à l'adresse de messagerie associée à chaque compte) sera
        personnalisé dès la première connexion. Il doit être composé d'au moins huit caractères contenant au moins une
        lettre majuscule, au moins une lettre minuscule, au moins un chiffre et au moins un caractère spécial. <b>
        <i>Exemple de mot de passe valide : Notaire!15 ou N1ot_aire5.</i></b> L'utilisateur doit conserver son mot de 
        passe qui lui sera utile pour tout accès à son compte. Le mot de passe doit être choisi par l'utilisateur de 
        façon qu'il ne puisse pas être deviné par un tiers. L'utilisateur s'engage à en préserver la confidentialité.
      </li>
      <li>Pour chaque compte utilisateur est associée une adresse de messagerie électronique valide. Cette adresse est
        utilisée pour la correspondance dans le cadre de l'utilisation de l'application, à savoir les réponses aux
        demandes effectuées par l'utilisateur. Cette adresse servira aussi pour l'envoi des codes de déverrouillage du
        compte et de réinitialisation du mot de passe.</li>
      <li>L'utilisation de l'application requiert une connexion et un navigateur internet. Le navigateur doit être
        configuré pour autoriser les cookies de session. Afin de garantir un bon fonctionnement de l'application, il est
        conseillé d'utiliser les versions les plus récentes des navigateur du marché.</li>
      <li>Le Service est optimisé pour un affichage de minimum 1280x800 en pixels.</li>

      <h3>Conservation des données de recherche</h3>
      <li>Chaque recherche lancée sur l'application sera automatiquement sauvegardée et stockée par le département. A
        chaque compte sera associée la liste des recherches lancées. Les services du département se réservent un droit
        de contrôle sur chacune de ces informations.</li> 
        
      <h3>Traitement des données à caractère personnel et droit d'accès</h3>
      <li>Le Département s'engage à prendre toutes les mesures nécessaires permettant de garantir la sécurité et la
        confidentialité des informations fournies par l'utilisateur.</li>
      <li>Le Département garantit aux utilisateurs de l'application les droits d'accès, de modification, de
        rectification, de suppression et d'opposition de toute donnée personnelle, prévus par la loi n° 78-17 du 6
        janvier 1978 relative à l'informatique aux fichiers et aux libertés.</li>
      <li>Le Département s'engage à n'opérer aucune commercialisation des informations et documents transmis par
        l'utilisateur au moyen de l'application ; il s'engage à ne pas les communiquer à des tiers, en dehors des cas
        prévus par la loi.</li>

      <h3>Modification et évolution de l'application</h3>
      <li>Le Département se réserve la liberté de faire évoluer, de modifier ou de suspendre, sans préavis,
        l'application pour des raisons de maintenance ou pour tout autre motif jugé nécessaire. L'utilisateur sera
        prévenu deux jours à l'avance pour une indisponibilité programmée de moins d'une demie journée et sera prévenu
        une semaine à l'avance pour une indisponibilité programmée d'un deux jours maximum. L'indisponibilité de
        l'application ne donne droit à aucune indemnité.</li>
      <li>Les CGU sont opposables pendant toute la durée d'utilisation de l'application et/ou jusqu'à ce que de
        nouvelles dispositions remplacent les présentes. Il appartient à l'utilisateur de s'informer des conditions
        générales d'utilisation de l'application en vigueur.</li>

      <h3>Support</h3>
      <li>L'application est accessible à toutes les études notariales du Rhône 24h/24 et 7j/7, le
        support est assuré uniquement durant les journées ouvrées. Le support de l'application se fait par mail à
        l'adresse {{config.mail_gestion}}</li>

      <h3>Formalités de déclarations</h3>
      <li>Conformément à la loi, le Service a fait l'objet d'une déclaration de traitement automatisé d'informations
        nominatives à la Commission nationale informatique et libertés (CNIL) et est consultable à l'adresse suivante :
        {{config.site_cil_departement}}</li>
      <li>Conformément aux dispositions du Référentiel général de sécurité, le Service a fait l'objet d''une
        homologation par l'autorité administrative compétente.</li>

      <h3>Dispositions légales relatives à la protection des bases de données</h3>
      <li>Les informations contenues sur le site sont protégées au titre de la loi du 1er juillet 1998 (transposant la
        Directive des Communautés Européennes n° 96/9/CE du 11 mars 1996 relative à la protection juridique des bases de
        données).</li>
      <li>Toute extraction ou réutilisation, tentative d'extraction ou tentative de réutilisation, totale ou partielle,
        sur tout support présent ou futur est interdite.</li>
    </ol>
  </div>
</div>
