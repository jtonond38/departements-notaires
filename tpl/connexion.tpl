﻿<form action="index.php" method="post">
  <table id="invit_connexion">
    <tr>
      <td colspan="2" height="150px"><p id="bienvenue_bien" align="justify">Bienvenue</p>
        <p id="bienvenue" align="justify">{{config.nom_application}} est destiné au strict usage professionnel des 
        études notariales en charge d'une succession, qui s'interrogent sur l'existence éventuelle d'une créance du 
        Département au titre de l'aide sociale.</p>
      </td>
    </tr>
    <tr>
      <td><label id="ident">Identifiant</label></td>
      <td><input type="text" class="input_connexion" id="login" name="login" onclick="viderLogin('login');" 
        required="required"></input>
      </td>
    </tr>
    <tr>
      <td><label id="ident">Mot de passe</label></td>
      <td><input type="password" class="input_connexion" name="pass" value="" required="required"></input></td>
    </tr>
    <tr>
      <td></td>
      <td><a class="index" href="index.php?index=mail_reinit">J'ai oublié mon mot de passe</a></td>
    </tr>
    <tr>
      <td></td>
      <td align="center"><input type="submit" id="submit_connexion" name="connexion" value="Connexion"></input></td>
    </tr>
  </table>
</form>
