{# Corps mail réponse recuperable et indus probable #}

<style>
 .corps{
    font-family:'verdana';
    font-size:80%%;
}
.signature{
    font-family:'verdana';
    font-size:75%;font-weight:bold;
}
</style>

<p class=corps>
    Maître,
    <br />
    <br /> Vous nous interrogez au sujet d'une éventuelle créance du département, 
    au nom de {{prenom}} {{nom}} et dont le décès est survenu le : {{date_deces}}.
    <br />
    <br /> Cette personne est connue du département, vous trouverez ci-joint la réponse de mes services.
    <br />
    <br /> Je vous prie d'agréer, Maître, mes courtoises salutations.
    <br />
    <br /> 
</p>
<p class="signature"> {{config.mail_signature | raw}}
</p>
